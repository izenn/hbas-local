## Usage
To start, 2 manual changes need to be made because they are environment specific:
1. edit `switch files/repos.json` and change CHANGEME to your ip
2. edit `docker-compose.yml` and change CHANGEME for LOCAL_IP to your ip

Once that is done you are ready to build. (this takes a while)  Wait until you can access the front end (node will start the front end on port 3000)
```
docker-compose up --build
```
After you are able to access the front-end, press ctrl-c once to gracefully stop the container

Now you can use the following to start the container.  Start is much quicker than build.
```
docker start hbas-local
```
to access the submitter, go to your `http://your-ip:5000` from there you can fill out the info, add an icon and banner, and upload your package.

### Tips
* version numbers should just be the number, a v will automatically be added. (ex don't use "v1.0" just use "1.0")

#### ToDo:
test ability to parse dot files in the packages
