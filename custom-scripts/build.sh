#!/bin/bash
apk add python3 yarn
git config --global --add safe.directory '*'
git config --global init.defaultBranch main

cp /custom-files/nginx-default.conf /config/nginx/site-confs/default.conf
cp /custom-files/php-local.ini /config/php
cd /config/www
git init 
git pull https://gitlab.com/izenn/submitter-local.git
cd /config/www/build
rm -f /config/www/build/.gitignore
git init
git pull https://gitlab.com/4TU/spinarak.git
rm -rf /config/www/build/DinoRunNX/ /config/www/build/Goldleaf/ /config/www/build/appstore/ /config/www/build/badPackage/
git init /config/node
cd /config/node
git pull https://gitlab.com/4TU/hbas-frontend.git
cp /custom-files/LibGet.js /config/node/src
cd /config
chown -R abc:abc *
perl -p -i -e "s/CHANGEME/$LOCAL_IP/g" $(grep -ril CHANGEME /config/*)
cd /config/node
yarn install
